from learner import *
from utils import *


if __name__ == "__main__":
    data = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST)

    learner = Learner(data)
    tree = learner.find_tree()
    rules = tree.find_rules(tree.root, data.attributes.target)

    tree_training_accuracy, rule_training_accuracy = Classifier.classify(data.training, tree, rules)
    tree_test_accuracy, rule_test_accuracy = Classifier.classify(data.testing, tree, rules)

    print "Decision tree used to classify tennis examples\n"
    tree.print_tree()

    print "Accuracy of tree on training data %f" % tree_training_accuracy
    print "Accuracy of tree on testing data %f" % tree_test_accuracy

    print "Rules used to classify tennis examples\n"
    tree.print_rules()

    print "Accuracy of rules on training data %f" % rule_training_accuracy
    print "Accuracy of rules on testing data %f" % rule_test_accuracy


