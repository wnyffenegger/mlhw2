from learner import *
from utils import *

import random

random.seed(42)

if __name__ == "__main__":

    # for each i corrupt 2 * i examples and display resulting accuracy for rules and post pruning rules
    for i in range(1, 11):
        # load fresh data
        data = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, True)

        # build range to choose indices in training data to corrupt
        # without replacement
        indices = [k for k in range(0, len(data.training))]

        # list possible values of target attribute to corrupt data by changing
        values = data.attributes.attributes[data.attributes.target]

        for j in range(0, i * 2):
            # Find index of example to corrupt
            choice = random.choice(indices)

            # Find current value of training example to change
            current_value = data.training[choice][data.attributes.target]

            # filter out current value from list of choices and choose value to corrupt to
            corruptible = [target_value for target_value in values if current_value != target_value]
            corrupted_value = random.choice(corruptible)

            # corrupt example
            data.training[choice][data.attributes.target] = corrupted_value

            # do not consider index again
            indices.remove(choice)

        learner = Learner(data)
        tree = learner.find_tree()

        rules = learner.find_rules()
        pruned = learner.find_and_prune_rules()

        _, rule_test_accuracy = Classifier.classify(data.testing, tree, rules)
        _, pruned_test_accuracy = Classifier.classify(data.testing, tree, pruned)

        print "%d corrupted examples. Accuracy of rules vs. post pruning rules %f vs. %f"\
              % ((2 * i), rule_test_accuracy, pruned_test_accuracy)


