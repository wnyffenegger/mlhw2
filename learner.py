from utils import *
import math

THRESHOLD = 0.000001


class Learner:

    def __init__(self, examples):
        """
        Initialize learner with attributes specification
        :type examples: Examples
        """
        self.attr = examples.attributes
        self.examples = examples

    @staticmethod
    def prob(subset, target, value):
        """
        Find entropy of a set of examples for the given target attributes with the given legal values.
        :param subset: list of examples to calculate Entropy for
        :type subset: list(dict)
        :param target: name of the target attribute
        :type target: str
        :param value: desired value for target attribute
        :type value: str
        :return: entropy of subset with respect to target attribute
        :rtype: float
        """
        matching = 0
        for example in subset:
            if example[target] == value:
                matching += 1
        return float(matching) / len(subset)

    @staticmethod
    def entropy(subset, target, values):
        """
        Find entropy of a set of examples for the given target attributes with the given legal values.
        :param subset: list of examples to calculate Entropy for
        :type subset: list(dict)
        :param target: name of the target attribute
        :type target: str
        :param values: list of values for target attribute
        :type values: list(str)
        :return: entropy of subset with respect to target attribute
        :rtype: float
        """

        entropy = 0
        for value in values:
            pvalue = Learner.prob(subset, target, value)

            if pvalue > THRESHOLD:
                entropy += pvalue * math.log(pvalue, 2)

        return - entropy

    def find_entropy(self, subset):
        return Learner.entropy(subset, self.attr.target, self.attr.attributes[self.attr.target])

    @staticmethod
    def split_by_attribute(subset, attribute, attribute_values):
        """
        Split subset by attribute values and return those splits
        :param subset:
        :type subset: list(dict)
        :param attribute: attribute to split examples on
        :type attribute: str
        :param attribute_values: possible values of attribute to split on
        :type attribute_values: list(str)
        :return: subsets of examples split by attribute value
        :rtype: dict(list(dict))
        """
        subsets = dict()

        for value in attribute_values:
            subsets[value] = list()

        for example in subset:
            subsets[example[attribute]].append(example)

        return subsets

    @staticmethod
    def gain(parent_examples, attribute, attribute_values, target, target_values):
        """
        Find the "information gain" with respect to the target attribute derived by splitting a set of examples on the designated
        attribute
        :param parent_examples: subset of examples from parent
        :type parent_examples: list(dict)
        :param attribute: attribute to split examples on
        :type attribute: str
        :param attribute_values: possible values of attribute to split on
        :type attribute_values: list(str)
        :param target: attribute to compute gain relative to
        :type target: str
        :param target_values: possible values of target attribute
        :type target_values: list(str)
        :return: difference in Entropy before and after splitting on attribute
        :rtype: float
        """

        entropy = Learner.entropy(parent_examples, target, target_values)

        # new subsets of examples formed by splitting on attribute
        children = Learner.split_by_attribute(parent_examples, attribute, attribute_values)

        # calculate entropy of the children
        child_entropy = 0
        for child in children.values():
            if len(child) > 0:
                child_entropy += (float(len(child)) / float(len(parent_examples))) * Learner.entropy(child, target, target_values)

        return entropy - child_entropy

    def find_gain(self, subset, attribute):
        return Learner.gain(subset, attribute, self.attr.attributes[attribute], self.attr.target, self.attr.attributes[self.attr.target])

    @staticmethod
    def similar(examples, attribute):
        """
        Given a list of examples and target attribute test whether all examples have the same value for the attribute.
        Expects length of examples to be greater than one

        :param examples: list of examples to consider
        :type examples: list(dict())
        :param attribute: attribute to consider values of
        :type attribute: str
        :return: true if all are same, false otherwise
        :rtype: bool
        """
        expected = examples[0][attribute]
        for example in examples:
            if example[attribute] != expected:
                return False
        return True

    def best(self, examples, attributes):

        best = attributes[0]
        gain = self.find_gain(examples, best)

        for attribute in attributes[1:]:
            attr_gain = self.find_gain(examples, attribute)

            if attr_gain > gain:
                best = attribute
                gain = attr_gain

        return best

    def id3(self, examples, attributes):
        """
        Builds a decision tree and returns the root node of that tree based on the provided examples, attributes, and
        target attribute. Assumes that all attributes are discrete and imposes no restrictions on the number
        of examples necessary to make a decision.

        :param examples: list of examples to consider
        :type examples: list(dict)
        :param attributes: set of attributes to consider making decisions on
        :type attributes: list(str)
        :return: list of elements
        :rtype: list(TreeNode | TreeLeaf)
        """

        target_attribute = self.attr.target
        target_values = self.attr.attributes[target_attribute]

        if Learner.similar(examples, target_attribute) or len(attributes) == 0:
            return [TreeLeaf(target_attribute, target_values, examples)]

        best = self.best(examples, attributes)
        best_values = self.attr.attributes[best]
        subsets = Learner.split_by_attribute(examples, best, best_values)

        children = []
        for value, subset in subsets.iteritems():
            child = TreeNode(best, value)
            if len(subset) > 0:
                child_attributes = [attr for attr in attributes if attr != best]
                sub_children = self.id3(subset, child_attributes)

                for sc in sub_children:
                    child.add_child(sc)
            else:
                child.add_child(TreeLeaf(target_attribute, target_values, examples))

            children.append(child)

        return children

    def find_tree(self):
        """
        Find a decision tree using ID3
        :return: root node of tree as part of DecisionTree instance
        :rtype: DecisionTree
        """
        root = TreeNode(attribute=None, value=None)
        attributes = self.attr.attributes.keys()
        attributes = [attr for attr in attributes if attr != self.attr.target]
        children = self.id3(self.examples.training, attributes)

        for child in children:
            root.add_child(child)

        return DecisionTree(self.attr, root)

    def find_rules(self):
        """
        Given a set of training examples return all learned rules without pruning
        :return: list of rules
        :rtype: list(Rule)
        """
        tree = self.find_tree()
        return DecisionTree.find_rules(tree.root, self.attr.target)

    def find_and_prune_rules(self, aggressive=False):
        """
        Find decision tree, convert to rules, conduct rule-post pruning, and rank rules
        :param aggressive: if True assume that rules with no examples in validation set matching have an accuracy of
        zero, basically assume that edge cases are errors. If false do not prune rule
        :type aggressive: bool
        :return: list of pruned rules ranked by accuracy
        :rtype: list(Rule)
        """
        rules = self.find_rules()

        pruned_rules = list()

        # prune each rule
        for rule in rules:

            index = len(rule.literals) - 1

            # for literal in each rule test if pruning literal will increase accuracy
            rule_accuracy = Classifier.find_accuracy(self.attr.target, self.examples.validation, rule)

            while index >= 0 and len(rule.literals) > 1:
                # copy rule and prune literal at index
                pruned_rule = rule.copy()
                pruned_rule.remove(index)

                # find accuracy of pruning
                prune_accuracy = Classifier.find_accuracy(self.attr.target, self.examples.validation, pruned_rule)

                # if the rule being tested is not applicable to any example in the validation set
                if abs(prune_accuracy + 1.0) < THRESHOLD:
                    # If pruning aggressively assume that leaves with no examples have an accuracy of zero
                    if aggressive:
                        prune_accuracy = 0
                    # Otherwise keep rule as is because there is no data to work with
                    else:
                        break

                # If difference isn't just because of properties of real numbers and prune is better
                if abs(prune_accuracy - rule_accuracy) > THRESHOLD and prune_accuracy > rule_accuracy:
                    # remove literal at index and set rule accuracy to prune accuracy
                    rule.remove(index)
                    rule_accuracy = prune_accuracy

                index -= 1

            pruned_rules.append((rule, rule_accuracy))

        pruned_rules.sort(key=lambda pr: pr[1], reverse=True)
        ranked = [prune_summary[0] for prune_summary in pruned_rules]
        return ranked


class Classifier:
    """
    Classify an example and find accuracy of rules based on training set
    """

    @staticmethod
    def matches(example, rule):
        """
        Determine if rule matches example (would rule be applied to example)
        :param example: example to test for match
        :type example: dict
        :param rule: rule to test for matching with dictionary
        :type rule: Rule
        :return: true if rule can be used on example
        :rtype: bool
        """

        # If at least one literal does not match rule return false
        for literal in rule.literals:
            if example[literal[0]] != literal[1]:
                return False

        return True

    @staticmethod
    def find_accuracy(target, validation_set, rule):
        """
        Find the accuracy of a single rule on an entire validation set
        :param target: attribute to match
        :type target: str
        :param validation_set: set of validation examples to test accuracy with
        :type validation_set: list(dict)
        :param rule: rule to test
        :type rule: Rule
        :return: accuracy of rule in predicting applicable examples in validation set, returns -1 if not example
        in validation set matches rule
        :rtype: float
        """

        correct = 0.0
        matches = 0

        for example in validation_set:

            # Only if rule matches an example do we test it for accuracy
            if Classifier.matches(example, rule):
                matches += 1

                if rule.value == example[target]:
                    correct += 1.0

        # if no data to evaluate return
        if matches == 0:
            return -1.0

        return correct / matches

    @staticmethod
    def classify_with_rules(example, rules):
        """
        Given a target attribute classify the example using the provided rules
        :param example: a single example to classify
        :type example: dict()
        :param rules:
        :type rules: list(Rule)
        :return: predicted classification, actual classification, whether classification was correct, and rule used
        :rtype: tuple(str, str, bool, Rule)
        """

        for rule in rules:
            if Classifier.matches(example, rule):
                return rule.value, example[rule.target], rule.value == example[rule.target], rule

        # Based on ID3 should never happen
        return None

    @staticmethod
    def classify_with_tree(example, tree):
        """
        Classify an example using the actual decision tree instead of rules
        :param example: an example to test with tree
        :type example: dict
        :param tree: tree to use for classification
        :type tree: DecisionTree
        :return: tuple(str, str, bool, Rule)
        """

        current = tree.root

        while current.__class__.__name__ != "TreeLeaf":

            if len(current.children) == 0:
                break
            elif len(current.children) == 1:
                current = current.children[0]
                break

            subsequent = None
            for child in current.children:
                if example[child.attribute] == child.value:
                    subsequent = child
                    break

            if subsequent is None:
                # If we ever get to this point then there is an error
                print "should be able to classify any example using tree error"
                break
            else:
                current = subsequent

        # build rule representation of path through tree
        rule = Rule(current.target)
        rule.value = current.majority

        selected = current.parent
        # For each decision made to get result
        while selected.parent is not None:
            rule.enqueue(selected.attribute, selected.value)
            selected = selected.parent

        return rule.value, example[rule.target], rule.value == example[rule.target], rule

    @staticmethod
    def classify(examples, tree, rules, display=False):
        """
        Compare classification of data by tree and rules
        :param examples: set of examples to classify
        :type examples: list(dict())
        :param tree: tree that rules are based off of
        :type tree: DecisionTree
        :param rules: set of rules to classify examples with
        :type rules: list(Rule)
        :param display: if true print out classifications as they occur
        :type display: bool
        :return: percentage of examples correctly classified
        :rtype: float
        """

        rules_correct = 0.0
        tree_correct = 0.0
        for example in examples:
            rule_result = Classifier.classify_with_rules(example, rules)
            tree_result = Classifier.classify_with_tree(example, tree)

            if rule_result[2]:
                rules_correct += 1.0

            if tree_result[2]:
                tree_correct += 1.0

            if display:
                # if rule correctly classified
                if rule_result[2]:
                    print "Correctly classified as %s by rule %s" % (rule_result[0], str(rule_result[3]))
                # if incorrectly classified
                else:
                    print "Incorrectly classified as %s should be %s by rule %s"\
                          % (rule_result[0], rule_result[1], str(rule_result[3]))

                if tree_result[2]:
                    print "Correctly classified as %s by tree as rule %s" % (rule_result[0], str(rule_result[3]))
                else:
                    print "Incorrectly classified as %s should be %s by tree as rule %s"\
                          % (tree_result[0], tree_result[1], str(tree_result[3]))

        return tree_correct / len(examples), rules_correct / len(examples)


def test_entropy():

    half = list()

    half.append({
        'test': 'a'
    })

    half.append({
        'test': 'b'
    })

    whole = list()

    whole.append({
        'test': 'a'
    })

    probability = Learner.prob(half, 'test', 'a')
    half_entropy = Learner.entropy(half, 'test', ['a', 'b'])
    whole_entropy = Learner.entropy(whole, 'test', ['a'])
    zero_entropy = Learner.entropy(whole, 'test', ['b'])

    if abs(probability - 0.5) > THRESHOLD:
        print "probability not calculated correctly"

    if abs(half_entropy - 1.0) > THRESHOLD:
        print "probability not calculated correctly should be 0.5"

    if abs(whole_entropy - 0.0) > THRESHOLD:
        print "entropy not calculated correctly should be 0"

    if abs(zero_entropy - 0.0) > THRESHOLD:
        print "entropy not calculated correctly should be 0"


def test_gain():

    half = list()

    half.append({
        'test': 'a'
    })

    half.append({
        'test': 'b'
    })

    gain = Learner.gain(half, 'test', ['a', 'b'], 'test', ['a', 'b'])

    if abs(gain - 1.0000) > THRESHOLD:
        print "gain not calculated correctly should be complete loss of uncertainty"


def test_classifier():

    rule = Rule("B", "True")
    rule.push("A > 2", "False")

    converse = Rule("B", "False")
    converse.push("A > 2", "True")

    match = {
        "A > 2": "False",
        "B": "True"
    }

    not_match = {
        "A > 2": "True",
        "B": "False"
    }

    incorrect = {
        "A > 2": "False",
        "B": "False"
    }

    if not Classifier.matches(match, rule):
        print "classifier is giving false negative matches of examples to rules"

    if Classifier.matches(not_match, rule):
        print "classifier is giving false positive matches of examples to rules"

    if abs(Classifier.find_accuracy("B", [match, not_match, incorrect], rule) - 0.5) > THRESHOLD:
        print "classifier is not correctly determining accuracy of a rule"

    if Classifier.classify_with_rules(match, [rule])[0] != 'True':
        print "classifier is not correctly classifying examples using rules"

    # if abs(Classifier.test([match, not_match, incorrect], [rule, converse], True) - (2.0 / 3.0)) > THRESHOLD:
    #     print "classifier is not correctly testing classifications of provided examples using provided rules"


def test_learner():

    plain_tennis = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST)
    plain_iris = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST)
    learner = Learner(plain_tennis)

    tree = learner.find_tree()
    print
    tree.print_tree()
    tree.print_rules()

    learner = Learner(plain_iris)
    tree = learner.find_tree()
    print
    tree.print_tree()
    tree.print_rules()


def test_rule_pruning():

    plain_iris = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, True)
    learner = Learner(plain_iris)
    tree = learner.find_tree()
    tree.print_rules()

    rules = learner.find_rules()

    for rule in rules:
        print rule

    pruned = learner.find_and_prune_rules()

    for rule in pruned:
        print rule

    before = Classifier.classify(plain_iris.testing, tree, rules, True)
    after = Classifier.classify(plain_iris.testing, tree, pruned, True)

    print "Accuracy before pruning %f" % before
    print "Accuracy after pruning %f" % after

