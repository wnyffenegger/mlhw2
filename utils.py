
CONTINUOUS = "continuous"

TENNIS_ATTR = 'data/tennis-attr.txt'
IRIS_ATTR = 'data/iris-attr.txt'
TEST_ATTR = 'data/test-attr.txt'

TENNIS_TEST = 'data/tennis-test.txt'
IRIS_TEST = 'data/iris-test.txt'

TENNIS_TRAINING = 'data/tennis-train.txt'
IRIS_TRAINING = 'data/iris-train.txt'
TEST_TRAINING = 'data/test-train.txt'


class Attributes:
    """
    Datatype containing attributes and their possible values
    """
    def __init__(self, attr_file=None):
        self.attributes = dict()            # possible values of attributes
        self.order = list()                 # order attributes appear in files

        if attr_file is not None:
            with open(attr_file, 'r') as attr:

                last_attr = None
                for line in attr:

                    line = line.strip()

                    # skip empty line
                    if len(line) == 0:
                        continue

                    attribute = line.split()
                    name = attribute[0]
                    last_attr = name
                    self.order.append(name)

                    if attribute[1] == CONTINUOUS:
                        self.attributes[name] = CONTINUOUS
                    else:
                        self.attributes[name] = list()
                        for val in attribute[1:]:
                            self.attributes[name].append(val)
                self.target = last_attr

    def values(self, name):
        """
        Get values of attribute
        :param name: valid name of an attribute
        :type name: str
        :return: list of possible values if discrete, otherwise str
        :rtype: list() | str
        """
        return self.attributes[name]

    def continuous(self, name):
        """
        Given an attribute name return whether the attribute is
        :param name: valid name of an attribute
        :type name: str
        :return: true if continuous false if discrete
        :rtype: bool
        """

        return self.attributes[name] == "continuous"

    def is_target(self, name):
        return name == self.target


class Examples:
    """
    Attribute data with corresponding examples
    """

    def __init__(self, attr_file, training_file, test_file, validation=False):
        self.attributes = Attributes(attr_file)
        self.training = self.load(training_file)
        self.testing = self.load(test_file)
        self.validation = None

        self.transform()

        if validation:
            self.split_validation()

    def split_validation(self):
        """
        Create validation set by splitting data from training set
        :return:
        """

        split = 2 * int(len(self.training) / 3)
        self.validation = self.training[split:]
        self.training = self.training[:split]

    def load(self, filename):
        """
        Given a file load examples according to specified attributes in expected order
        :param filename:
        :return:
        """
        examples = list()

        with open(filename, 'r') as ex:

            for line in ex:

                line = line.strip()
                example = dict()
                # split line into tokens and evaluate each as an assigned value to an attribute
                for attr, value in zip(self.attributes.order, line.split()):

                    if self.attributes.continuous(attr):
                        example[attr] = float(value)
                    else:
                        example[attr] = value
                examples.append(example)

        return examples

    @staticmethod
    def find_splits(training, attribute, target):
        """
        Given a set of training data and an attribute find all logical splits of the data into binary attributes
        :param training: set of training examples
        :type training: list(dict)
        :param attribute: attribute name to find splits on
        :type attribute: str
        :param target: target attribute name which goal is
        :type target: str
        :return: list of floats where logical binary splits occur
        :rtype: list(float)
        """
        values = [(example[attribute], example[target]) for example in training]
        values.sort(key=lambda v: v[0])         # sort on first value i tuple which is target attribute value

        splits = list()
        predecessor = values[0]
        for v in values[1:]:
            if v[1] != predecessor[1]:
                splits.append((v[0] + predecessor[0]) / 2.0)
            predecessor = v

        return splits

    def transform(self):
        """
        Create new attributes to split on by taking continuous attributes and turning them into sets of binary attrs.

        Does so for all training and testing examples
        """
        updated_attributes = Attributes()
        updated_attributes.target = self.attributes.target
        updated_examples = self.training
        updated_tests = self.testing

        for original in self.attributes.order:

            # If continuous attribute try to find splits
            if self.attributes.continuous(original):
                splits = Examples.find_splits(self.training, original, self.attributes.target)

                # For each potential split define new attribute
                # and give True/False value for that attribute to every example and test
                for split in splits:
                    new_attribute = "%s > %.3f" % (original, split)

                    updated_attributes.order.append(new_attribute)
                    updated_attributes.attributes[new_attribute] = ['True', 'False']

                    for example in updated_examples:
                        example[new_attribute] = str(example[original] > split)

                    for test in updated_tests:
                        test[new_attribute] = str(test[original] > split)

                # Delete original attribute from tests and examples
                for example in updated_examples:
                    del example[original]

                for test in updated_tests:
                    del test[original]

            else:
                # Include discrete attributes as is
                updated_attributes.order.append(original)
                updated_attributes.attributes[original] = self.attributes.attributes[original]

        # Finalize updates
        self.attributes = updated_attributes
        self.training = updated_examples
        self.testing = updated_tests


class Rule:
    """
    Abstraction of a rule to be pruned
    """

    def __init__(self, target, value=None):
        """
        Create rule which culminates in a particular value of a target attribute
        :param target: name of the target attribute
        :type target: str
        :param value:
        """
        self.literals = list()
        self.target = target
        self.value = value

    def push(self, attribute, value):
        """Add component to back of list"""
        self.literals.append((attribute, value))

    def enqueue(self, attribute, value):
        """Add component to front of list"""
        self.literals.insert(0, (attribute, value))

    def remove(self, index):
        """
        Remove item at index
        :param index:  valid index for literal list
        :type index: int
        :return:
        """
        del self.literals[index]

    def copy(self):
        """
        Create copy of rule
        :return: a complete copy of rule
        :rtype: Rule
        """
        cpy = Rule(self.target, self.value)

        for literal in self.literals:
            cpy.push(literal[0], literal[1])

        return cpy

    def __str__(self):
        return ", ".join(["%s = %s" % (attr, val) for attr, val in self.literals])\
               + " => " + "%s = %s" % (self.target, self.value)


class TreeLeaf:

    def __init__(self, target, values, examples):
        """
        End of path through tree giving
        :param target: name of target attribute
        :type target: str
        :param values: list of bins
        :type values: list(str)
        :param examples:
        """
        self.target = target
        self.voting = dict()
        self.majority = values[0]

        for value in values:
            self.voting[value] = 0

        for example in examples:
            self.voting[example[target]] += 1

        for key in self.voting.keys():
            if self.voting[self.majority] < self.voting[key]:
                self.majority = key

    def __str__(self):

        values = sorted(self.voting.keys())
        formatted_votes = ["%s : %d" % (key, self.voting[key]) for key in values]
        return "%s = %s (%s)" % (self.target, self.majority, ", ".join(formatted_votes))


class TreeNode:
    """
    Node for a tree with a parent, children, and lists of assigned, unassigned and an assignment of values
    """
    def __init__(self, attribute, value, parent=None):
        """
        Create a node with a list of assigned attributes and unassigned attributes
        :param attribute: attribute name that is being split on
        :type attribute: str | None
        :param value: value
        :type value: str | None

        :param parent: parent Node of current
        :type parent: TreeNode
        """

        self.parent = parent
        self.children = list()

        self.attribute = attribute
        self.value = value

    def add_child(self, child):
        """
        :param child: list of child nodes or leaves
        :type child: TreeNode | TreeLeaf
        """
        child.parent = self
        self.children.append(child)

    def find_child(self, example):
        """
        Find the child corresponding to an example
        :param example: an example with all attributes including target attribute
        :type example: dict
        :return:
        """
        child = None

        # find child with matching value to example
        for c in self.children:
            if example[c.attribute] == c.value:
                child = c
                break

        return child

    def __str__(self):
        if self.attribute is not None:
            return '%s = %s (%d child node(s))' % (self.attribute, self.value, len(self.children))
        else:
            return 'root (%d child node(s))' % len(self.children)


class DecisionTree:
    """
    Decision tree class with parent node and children
    """

    def __init__(self, attributes, root):
        """
        :param attributes: set of attributes tree is based off of
        :type attributes: Attributes
        :param root: root node of already existing tree or tree being created
        """
        self.attributes = attributes
        self.root = root

    @staticmethod
    def find_leaves(root):
        """
        :param root: root of a decision tree
        :type root: TreeNode
        :return:
        """
        leaves = list()
        queue = list()
        queue.append(root)

        while not len(queue) == 0:
            popped = queue.pop(0)

            if popped.__class__.__name__ == 'TreeLeaf':
                leaves.append(popped)
            elif len(popped.children) == 0:
                leaves.append(popped)
            else:
                for child in popped.children:
                    queue.append(child)
        return leaves

    @staticmethod
    def find_rules(root, target):
        """
        Find all partial or complete rules (for the sake of testing partial rules)
        :param root: root of a decision tree
        :type root: TreeNode
        :param target: target attribute name
        :type target: str
        :return: list of rules
        :rtype: list(Rule)
        """
        leaves = DecisionTree.find_leaves(root)
        rules = list()
        for leaf in leaves:
            selected = leaf

            # If leaf set the target value determined by decision tree otherwise leave unknown
            rule = Rule(target)
            if selected.__class__.__name__ == "TreeLeaf":
                rule.value = leaf.majority
                selected = leaf.parent

            # For each decision made to get result
            while selected.parent is not None:
                rule.enqueue(selected.attribute, selected.value)
                selected = selected.parent

            rules.append(rule)
        return rules

    @staticmethod
    def display_rules(root, target):
        """
        Find all partial or complete rules (for the sake of testing partial rules)
        :param root: root of a decision tree
        :type root: TreeNode
        :param target: target attribute name
        :type target: str
        """
        rules = DecisionTree.find_rules(root, target)

        for rule in rules:
            print rule

    def print_rules(self):
        DecisionTree.display_rules(self.root, self.attributes.target)

    @staticmethod
    def display_tree(node, level):
        """
        Print out a node with indentation based on depth in the tree
        :param node:
        :type node: TreeNode | TreeLeaf
        :param level: level into the tree (0 is root)
        :type level: int
        """
        if level > 0:
            print "%s %s" % ('\t'.join(['|'] * level), str(node))

        if node.__class__.__name__ == 'TreeNode':
            for c in node.children:
                DecisionTree.display_tree(c, level + 1)

    def print_tree(self):
        DecisionTree.display_tree(self.root, 0)


def test_attributes():

    tennis = Attributes(TENNIS_ATTR)
    iris = Attributes(IRIS_ATTR)

    if len(tennis.order) != 5:
        print "Order incorrect, expected 5 got %d" % len(tennis.order)

    if not iris.continuous(iris.order[0]):
        print "All Iris attributes should be continuous, %s isn't" % iris.order[0]

    if not iris.is_target(iris.order[-1]):
        print "Last attribute %s should be target but is not" % iris.order[-1]

    if not len(tennis.values(tennis.order[0])) == 3:
        print "Not reading correct number of attributes expected %d got %d" % (3, len(tennis.values(tennis.order[0])))


def test_splitting():

    split = Examples(TEST_ATTR, TEST_TRAINING, TEST_TRAINING)
    attributes = split.attributes
    training = split.training
    test = split.testing

    if len(attributes.attributes.keys()) != 5:
        print "Failed to split attributes correctly"

    if len(training[0].keys()) != 5:
        print "Failed to reformat training examples correctly"

    if len(test[0].keys()) != 5:
        print "Failed to reformat test examples correctly"

    first = training[0]

    if first['A > 2.500'] == 'True':
        print "Did not do comparison correctly for newly formed attributes"


def test_examples():

    validated_tennis = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST, True)
    plain_tennis = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST)

    if len(plain_tennis.testing) != 4:
        print "did not read all tennis test examples"

    if len(plain_tennis.training) != 10:
        print "did not read all tennis training examples"

    if len(validated_tennis.training) != 6 or len(validated_tennis.testing) != 4:
        print "did not correctly split tennis training into validation and training"

    validate_iris = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, True)

    plain_iris = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST)

    if len(plain_iris.testing) != 50:
        print "did not read all tennis test examples"

    if len(plain_iris.training) != 100:
        print "did not read all tennis training examples"

    if len(validate_iris.training) != 66:
        print "did not correctly split iris training into validation and training"

    plain_iris.transform()


def test_nodes_and_leaves():

    examples = Examples(TEST_ATTR, TEST_TRAINING, TEST_TRAINING)
    leaf = TreeLeaf('C', ['True', 'False'], examples.training)

    unassigned = examples.attributes.attributes.keys()
    root = TreeNode(None, None)

    unassigned.remove('B')
    btrue = TreeNode('B', 'True')
    bfalse = TreeNode('B', 'False')

    root.add_child(btrue)
    root.add_child(bfalse)

    if not leaf.majority == 'True':
        print 'failed to vote correctly'

    first = examples.training[0]

    child = root.find_child(first)
    if str(child) != 'B = True (0 children)':
        print 'failed to find correct child of node'

    print child
    print leaf      # see if to string makes sense


def test_tress():
    print '\n'

    examples = Examples(TEST_ATTR, TEST_TRAINING, TEST_TRAINING)

    unassigned = examples.attributes.attributes.keys()
    root = TreeNode(None, None)

    unassigned.remove('B')

    # Set of side of tree where B is true
    btrue = TreeNode('B', 'True')
    a0true = TreeNode('A > 0.500', 'True')
    a0false = TreeNode('A > 0.500', 'False')
    btrue.add_child(a0false)
    btrue.add_child(a0true)

    # Set up side of tree where B is false
    bfalse = TreeNode('B', 'False')
    a2true = TreeNode('A > 2.500', 'True')
    a2false = TreeNode('A > 2.500', 'False')
    bfalse.add_child(a2false)
    bfalse.add_child(a2true)

    training = examples.training
    selection = [training[3], training[7], training[8], training[9]]
    leaf = TreeLeaf('C', examples.attributes.attributes['C'],selection)
    a2true.add_child(leaf)

    root.add_child(btrue)
    root.add_child(bfalse)

    tree = DecisionTree(examples.attributes, root)
    tree.print_tree()
    tree.print_rules()


if __name__ == "__main__":
    test_attributes()
    test_splitting()
    test_examples()
    test_nodes_and_leaves()
    test_tress()
