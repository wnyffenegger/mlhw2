# ML HW2 Decision Trees

## Structure

* data: all attribute, training, and test data including a test-attr.txt and test-train.txt set of simple examples
used to test the performance of each module of code
* utils.py: all necessary utility classes for performing the decision tree algorithm including methods
for nodes and tress, finding rules, and printing information
* learner.py: contains id3 and classification code including rule post pruning
* testTennis.py: prints the tree, tree accuracy, tree converted to rules, and rule accuracy
* testIris.py: prints the tree, tree accuracy, pruned rules, and accuracy of pruned rules
* testIrisNoisy.py prints the accuracy of initial rules vs. post-pruned rules for incrementally
increasing levels of noise (2%, 4%, ... , 20%)
* testIrisNoisyAggressivePruning: variant of post-rule pruning where rules with no matching examples
in the validation set are assumed to have accuracy zero. Just interesting to try.

## Code Details

Listed below are relevant classes, a brief description of each class, and how
each class contributes to the assignment

* utils.py
    1. Attributes: abstracts list of attributes for an example with a set target and the ability
    to return this list of valid values for an attribute. Reads in attributes from a file
    2. Examples: encapsulation of attributes, training set, validation set (as necessary), and testing set.
    Loads examples from provided files, stores examples as lists of dict objects, and converts
    continuous valued attributes to a series of binary attributes
    3. Rule: abstraction of a rule with a classification and list of conditions implying that classification.
    List is a tuple of attribute, value pairs
    4. TreeLeaf: leaf of a DecisionTree where a classification is made. Automatically performs majority voting
    based on provided examples.
    5. TreeNode: a node in the decision tree with at least one child. Matches an example to a child node and
    maintains notion of a parent.
    6. DecisionTree: abstraction of a tree with methods to build rule representation of tree, display tree as pre-order
    traversal, and display rules
* learner.py
    1. Learner: contains all methods necessary for performing ID3 including calculating
    Entropy, Information Gain, best attribute to split by, etc. Also includes method to post-prune rules
    2. Classifier: helper methods to find the accuracy of a rule, determine whether a rule
    matches an example, and perform classification of an example using either a tree, a set of
    rules, or both.

## Running

```bash

cd mlhw2

python testTennis.py

python testIris.py

python testIrisNoisy.py

```